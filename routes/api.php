<?php

use App\Http\Controllers\CustomerController;
use App\Http\Controllers\ItemController;
use App\Http\Controllers\ItemSalesController;
use App\Http\Controllers\SalesController;
use Illuminate\Support\Facades\Route;


Route::group(['prefix' => 'v1'], function(){
    Route::apiResource('customers', CustomerController::class);
    Route::apiResource('items', ItemController::class);
    Route::apiResource('sales', SalesController::class);
    Route::apiResource('item-sales', ItemSalesController::class);
});