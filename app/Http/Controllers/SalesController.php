<?php

namespace App\Http\Controllers;

use App\Http\Resources\SalesResource;
use App\Models\Sales;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Response;

class SalesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sales = Sales::get();
        return response(SalesResource::collection($sales), Response::HTTP_OK);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $messages = [
            'date.required' => 'Tanggal harus diisi',
            'customer_id.required' => 'Pelanggan harus diisi',
            'item_sales_id.required' => 'Nota harus diisi'
        ];

        $rules = [
            'date' => 'required|date',
            'customer_id' => 'required',
            'item_sales_id' => 'required|numeric'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return response()->json([
                'error' => $validator->errors()
            ], 400);
        }

        $sales = Sales::create([
            'date' => $request->date,
            'customer_id' => $request->customer_id,
            'item_sales_id' => $request->item_sales_id
        ]);

        return response($sales, Response::HTTP_CREATED);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $sales = Sales::findOrFail($id);
        return response(new SalesResource($sales), Response::HTTP_OK);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $sales = Sales::findOrFail($id);
        $sales->update([
            'date' => $request->date,
            'customer_id' => $request->customer_id,
            'item_sales_id' => $request->item_sales_id
        ]);

        return response($sales, Response::HTTP_ACCEPTED);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Sales::destroy($id);
        return response(null, Response::HTTP_NO_CONTENT);
    }
}
