<?php

namespace App\Http\Controllers;

use App\Http\Resources\ItemSalesResource;
use App\Models\ItemSales;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Response;

class ItemSalesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $item_sales = ItemSales::orderBy('id', 'desc')->get();
        return response(ItemSalesResource::collection($item_sales), Response::HTTP_OK);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $messages = [
            'item_id.required' => 'Nama harus diisi',
            'qty.required' => 'Kategori harus diisi'
        ];

        $rules = [
            'item_id' => 'required',
            'qty' => 'required'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return response()->json([
                'error' => $validator->errors()
            ], 400);
        }

        $item_sales = ItemSales::create([
            'item_id' => $request->item_id,
            'qty' => $request->qty,
        ]);

        return response(new ItemSalesResource($item_sales), Response::HTTP_CREATED);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $item_sales = ItemSales::findOrFail($id);
        return response(new ItemSalesResource($item_sales), Response::HTTP_OK);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $item_sales = ItemSales::findOrFail($id);
        $item_sales->update($request->only('item_id', 'qty'));
        return response(new ItemSalesResource($item_sales), Response::HTTP_ACCEPTED);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        ItemSales::destroy($id);
        return response(null, Response::HTTP_NO_CONTENT);
    }
}
