<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Sales extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }

    public function item_sales()
    {
        return $this->belongsTo(ItemSales::class, 'item_sales_id');
    }

    public function getDate()
    {
        return Carbon::parse($this->date)->translatedFormat('l, d F Y');
    }

    public function getSubtotal()
    {
        $subtotal = DB::table('item_sales')
            ->join('items', 'item_sales.item_id', '=', 'items.id')
            ->where('item_sales.id', $this->item_sales_id)
            ->first();

        return $subtotal->qty * $subtotal->price;
    }
}
